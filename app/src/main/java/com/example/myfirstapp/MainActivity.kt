package com.example.myfirstapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView

//import kotlinx.android.synthetic.main.activity_display_message.textView
//import android.myfirstapp.MainActivity

//import android.view.View
//import android.view.View.OnClickListener
//import android.widget.Button
//import kotlinx.android.synthetic.main.activity_main.*


const val EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE"
//const val EXTRA_MESSAGE2 = "com.example.myfirstapp.MESSAGE"

class MainActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun showMessage(view: View)
    {
//        Body for the method
        val textMessage = "My first application Android \n\n\r Development by Jaime Montoya"
        val intent = Intent(this, DisplayMessageActivity::class.java).apply {
            putExtra(EXTRA_MESSAGE, textMessage)
        }

        startActivity(intent)
    }


    fun oldMayor(view: View)
    {
        val objName = findViewById<EditText>(R.id.txtName)
        val strName = "Your name: " + objName.text.toString()

        val objAge = findViewById<EditText>(R.id.nbrAge)
        val strAge: String

        val objFemaleRadio = findViewById<EditText>(R.id.optFemale) as RadioButton
        val objFemale = findViewById<EditText>(R.id.optFemale) as TextView
        val objMale = findViewById<EditText>(R.id.optMale) as TextView

        val strSex: String

        if (objFemaleRadio.isChecked()){
            strSex = objFemale.text.toString()
        }else{
            strSex = objMale.text.toString()
        }

        val message: String
        val intAge: Int

        if (objAge.text.toString() == ""){
            intAge = 0
            strAge = "Your age: 0"
        }else{
            intAge = objAge.text.toString().toInt()
            strAge = "Your age: " + objAge.text.toString()
        }

        if (intAge >= 18){
            message = "of legal age"
        }else{
            message = "a minor"
        }

        val intent = Intent(this, DisplayPersonalDataActivity::class.java).apply {
            putExtra("name", strName)
            putExtra("age", strAge)
            putExtra("sex", strSex)
            putExtra("result", message)
        }

//        Another way to do the same above
//        intent.putExtra("name", strName)
//        intent.putExtra("age", strAge)

        startActivity(intent)
    }

}
