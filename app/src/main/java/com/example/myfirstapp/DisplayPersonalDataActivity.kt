package com.example.myfirstapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class DisplayPersonalDataActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_personal_data)

        // Get the Intent that started this activity and extract the string
        val name = intent.getStringExtra("name")
        val age = intent.getStringExtra("age")
        val sex = intent.getStringExtra("sex")
        val result = intent.getStringExtra("result")

        // Capture the layout's TextView and set the string as its text
        val textView = findViewById<TextView>(R.id.txtName).apply {
            text = name
        }

        val textView2 = findViewById<TextView>(R.id.txtAge).apply {
            text = age
        }

        val textView3 = findViewById<TextView>(R.id.txtSex).apply {
            text = "Your sex: " + sex
        }

        val textView4 = findViewById<TextView>(R.id.txtResult).apply {
            text = "(You are " + result + ")"
        }
    }

}
